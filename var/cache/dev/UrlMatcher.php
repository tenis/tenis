<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/' => [[['_route' => 'homepage', 'path' => '/tr', 'permanent' => true, 'keepQueryParams' => true, 'keepRequestMethod' => true, '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::urlRedirectAction'], null, null, null, false, false, null]],
        '/tr/api/login_check' => [[['_route' => 'login_check', 'path' => '/api/login_check', 'permanent' => true, 'keepQueryParams' => true, 'keepRequestMethod' => true, '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::urlRedirectAction'], null, null, null, false, false, null]],
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/api/login_check' => [[['_route' => 'api_login_check'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/(tr)(?'
                    .'|(*:15)'
                    .'|/(?'
                        .'|a(?'
                            .'|pi(?'
                                .'|/mobil\\-function/(?'
                                    .'|test(*:59)'
                                    .'|get\\-ip\\-address(*:82)'
                                    .'|register(*:97)'
                                    .'|change\\-password(*:120)'
                                .')'
                                .'|(?:/(index)(?:\\.([^/]++))?)?(*:157)'
                                .'|/(?'
                                    .'|docs(?:\\.([^/]++))?(*:188)'
                                    .'|graphql(?'
                                        .'|(*:206)'
                                        .'|/graph(?'
                                            .'|iql(*:226)'
                                            .'|ql_playground(*:247)'
                                        .')'
                                    .')'
                                    .'|contexts/(.+)(?:\\.([^/]++))?(*:285)'
                                    .'|user(?'
                                        .'|_profiles(?'
                                            .'|(?:\\.([^/]++))?(?'
                                                .'|(*:330)'
                                            .')'
                                            .'|/([^/\\.]++)(?:\\.([^/]++))?(?'
                                                .'|(*:368)'
                                            .')'
                                        .')'
                                        .'|s(?'
                                            .'|(?:\\.([^/]++))?(?'
                                                .'|(*:400)'
                                            .')'
                                            .'|/([^/\\.]++)(?:\\.([^/]++))?(?'
                                                .'|(*:438)'
                                            .')'
                                        .')'
                                    .')'
                                    .'|l(?'
                                        .'|og_users(?'
                                            .'|(?:\\.([^/]++))?(?'
                                                .'|(*:482)'
                                            .')'
                                            .'|/([^/\\.]++)(?:\\.([^/]++))?(?'
                                                .'|(*:520)'
                                            .')'
                                        .')'
                                        .'|eagues(?'
                                            .'|(?:\\.([^/]++))?(?'
                                                .'|(*:557)'
                                            .')'
                                            .'|/([^/\\.]++)(?:\\.([^/]++))?(?'
                                                .'|(*:595)'
                                            .')'
                                        .')'
                                    .')'
                                .')'
                            .')'
                            .'|dmin(?'
                                .'|(*:615)'
                                .'|/league(?'
                                    .'|(*:633)'
                                    .'|/(?'
                                        .'|new(*:648)'
                                        .'|([^/]++)(?'
                                            .'|(*:667)'
                                            .'|/edit(*:680)'
                                            .'|(*:688)'
                                        .')'
                                    .')'
                                .')'
                            .')'
                        .')'
                        .'|test(?'
                            .'|2/(?'
                                .'|get\\-ip\\-address(*:729)'
                                .'|test2(*:742)'
                            .')'
                            .'|(*:751)'
                        .')'
                        .'|js/routing(?:\\.(js|json))?(*:786)'
                        .'|media/cache/resolve/(?'
                            .'|([A-z0-9_-]*)/rc/([^/]++)/(.+)(*:847)'
                            .'|([A-z0-9_-]*)/(.+)(*:873)'
                        .')'
                        .'|sg/datatables/edit/field(*:906)'
                    .')'
                .')'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(?'
                        .'|(*:950)'
                    .')'
                    .'|wdt/([^/]++)(*:971)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:1017)'
                            .'|router(*:1032)'
                            .'|exception(?'
                                .'|(*:1053)'
                                .'|\\.css(*:1067)'
                            .')'
                        .')'
                        .'|(*:1078)'
                    .')'
                .')'
                .'|/(tr)/a(?'
                    .'|dmin/(?'
                        .'|log(?'
                            .'|in(?'
                                .'|(*:1118)'
                                .'|_check(*:1133)'
                            .')'
                            .'|out(*:1146)'
                        .')'
                        .'|profile(?'
                            .'|(*:1166)'
                            .'|/(?'
                                .'|edit(*:1183)'
                                .'|change\\-password(*:1208)'
                            .')'
                        .')'
                        .'|re(?'
                            .'|gister(?'
                                .'|(*:1233)'
                                .'|/c(?'
                                    .'|heck\\-email(*:1258)'
                                    .'|onfirm(?'
                                        .'|/([^/]++)(*:1285)'
                                        .'|ed(*:1296)'
                                    .')'
                                .')'
                            .')'
                            .'|setting/(?'
                                .'|re(?'
                                    .'|quest(*:1329)'
                                    .'|set/([^/]++)(*:1350)'
                                .')'
                                .'|send\\-email(*:1371)'
                                .'|check\\-email(*:1392)'
                            .')'
                        .')'
                    .')'
                    .'|pi(*:1406)'
                .')'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        15 => [[['_route' => 'home_page_action', '_controller' => 'App\\Controller\\Front\\HomePageController::homePageAction'], ['_locale'], ['GET' => 0, 'POST' => 1], null, true, true, null]],
        59 => [[['_route' => 'mobil_test', '_controller' => 'App\\Controller\\Front\\MobilFunctionController::test'], ['_locale'], ['POST' => 0], null, false, false, null]],
        82 => [[['_route' => 'mobil_get_ip_address', '_controller' => 'App\\Controller\\Front\\MobilFunctionController::getIpAddress'], ['_locale'], ['GET' => 0], null, false, false, null]],
        97 => [[['_route' => 'mobil_mobil_register', '_controller' => 'App\\Controller\\Front\\MobilFunctionController::register'], ['_locale'], ['POST' => 0], null, false, false, null]],
        120 => [[['_route' => 'mobil_mobil_change_password', '_controller' => 'App\\Controller\\Front\\MobilFunctionController::changePassword'], ['_locale'], ['POST' => 0], null, false, false, null]],
        157 => [[['_route' => 'api_entrypoint', '_controller' => 'api_platform.action.entrypoint', '_format' => '', '_api_respond' => 'true', 'index' => 'index'], ['_locale', 'index', '_format'], null, null, false, true, null]],
        188 => [[['_route' => 'api_doc', '_controller' => 'api_platform.action.documentation', '_format' => '', '_api_respond' => 'true'], ['_locale', '_format'], null, null, false, true, null]],
        206 => [[['_route' => 'api_graphql_entrypoint', '_controller' => 'api_platform.graphql.action.entrypoint', '_graphql' => true], ['_locale'], null, null, false, false, null]],
        226 => [[['_route' => 'api_graphql_graphiql', '_controller' => 'api_platform.graphql.action.graphiql', '_graphql' => true], ['_locale'], null, null, false, false, null]],
        247 => [[['_route' => 'api_graphql_graphql_playground', '_controller' => 'api_platform.graphql.action.graphql_playground', '_graphql' => true], ['_locale'], null, null, false, false, null]],
        285 => [[['_route' => 'api_jsonld_context', '_controller' => 'api_platform.jsonld.action.context', '_format' => 'jsonld', '_api_respond' => 'true'], ['_locale', 'shortName', '_format'], null, null, false, true, null]],
        330 => [
            [['_route' => 'api_user_profiles_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\UserProfile', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_collection_operation_name' => 'get'], ['_locale', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_user_profiles_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\UserProfile', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_collection_operation_name' => 'post'], ['_locale', '_format'], ['POST' => 0], null, false, true, null],
        ],
        368 => [
            [['_route' => 'api_user_profiles_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\UserProfile', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'get'], ['_locale', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_user_profiles_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\UserProfile', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'delete'], ['_locale', 'id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_user_profiles_put_item', '_controller' => 'api_platform.action.put_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\UserProfile', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'put'], ['_locale', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        400 => [
            [['_route' => 'api_users_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_collection_operation_name' => 'get'], ['_locale', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_users_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_collection_operation_name' => 'post'], ['_locale', '_format'], ['POST' => 0], null, false, true, null],
        ],
        438 => [
            [['_route' => 'api_users_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'get'], ['_locale', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_users_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'delete'], ['_locale', 'id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_users_put_item', '_controller' => 'api_platform.action.put_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\User', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'put'], ['_locale', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        482 => [
            [['_route' => 'api_log_users_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\LogEntity\\LogUser', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_collection_operation_name' => 'get'], ['_locale', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_log_users_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\LogEntity\\LogUser', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_collection_operation_name' => 'post'], ['_locale', '_format'], ['POST' => 0], null, false, true, null],
        ],
        520 => [
            [['_route' => 'api_log_users_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\LogEntity\\LogUser', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'get'], ['_locale', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_log_users_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\LogEntity\\LogUser', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'delete'], ['_locale', 'id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_log_users_put_item', '_controller' => 'api_platform.action.put_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\LogEntity\\LogUser', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'put'], ['_locale', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        557 => [
            [['_route' => 'api_leagues_get_collection', '_controller' => 'api_platform.action.get_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\League', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_collection_operation_name' => 'get'], ['_locale', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_leagues_post_collection', '_controller' => 'api_platform.action.post_collection', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\League', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_collection_operation_name' => 'post'], ['_locale', '_format'], ['POST' => 0], null, false, true, null],
        ],
        595 => [
            [['_route' => 'api_leagues_get_item', '_controller' => 'api_platform.action.get_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\League', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'get'], ['_locale', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'api_leagues_delete_item', '_controller' => 'api_platform.action.delete_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\League', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'delete'], ['_locale', 'id', '_format'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'api_leagues_put_item', '_controller' => 'api_platform.action.put_item', '_format' => null, '_stateless' => null, '_api_resource_class' => 'App\\Entity\\League', '_api_identifiers' => ['id'], '_api_has_composite_identifier' => false, '_api_item_operation_name' => 'put'], ['_locale', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        615 => [[['_route' => 'admin_home_page', '_controller' => 'App\\Controller\\Admin\\AdminHomePageController::adminHomePage'], ['_locale'], ['GET' => 0, 'POST' => 1], null, true, false, null]],
        633 => [[['_route' => 'league_index', '_controller' => 'App\\Controller\\Admin\\LeagueController::index'], ['_locale'], ['GET' => 0], null, true, false, null]],
        648 => [[['_route' => 'league_new', '_controller' => 'App\\Controller\\Admin\\LeagueController::new'], ['_locale'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        667 => [[['_route' => 'league_show', '_controller' => 'App\\Controller\\Admin\\LeagueController::show'], ['_locale', 'id'], ['GET' => 0], null, false, true, null]],
        680 => [[['_route' => 'league_edit', '_controller' => 'App\\Controller\\Admin\\LeagueController::edit'], ['_locale', 'id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        688 => [[['_route' => 'league_delete', '_controller' => 'App\\Controller\\Admin\\LeagueController::delete'], ['_locale', 'id'], ['DELETE' => 0], null, false, true, null]],
        729 => [[['_route' => 'test_2_mobil_get_ip_address', '_controller' => 'App\\Controller\\Front\\Test2Controller::getIpAddress'], ['_locale'], ['GET' => 0], null, false, false, null]],
        742 => [[['_route' => 'test_2_test2', '_controller' => 'App\\Controller\\Front\\Test2Controller::index'], ['_locale'], null, null, false, false, null]],
        751 => [[['_route' => 'test', '_controller' => 'App\\Controller\\Front\\TestController::index'], ['_locale'], null, null, false, false, null]],
        786 => [[['_route' => 'fos_js_routing_js', '_controller' => 'fos_js_routing.controller:indexAction', '_format' => 'js'], ['_locale', '_format'], ['GET' => 0], null, false, true, null]],
        847 => [[['_route' => 'liip_imagine_filter_runtime', '_controller' => 'Liip\\ImagineBundle\\Controller\\ImagineController::filterRuntimeAction'], ['_locale', 'filter', 'hash', 'path'], ['GET' => 0], null, false, true, null]],
        873 => [[['_route' => 'liip_imagine_filter', '_controller' => 'Liip\\ImagineBundle\\Controller\\ImagineController::filterAction'], ['_locale', 'filter', 'path'], ['GET' => 0], null, false, true, null]],
        906 => [[['_route' => 'sg_datatables_edit', '_controller' => 'Sg\\DatatablesBundle\\Controller\\DatatableController::editAction'], ['_locale'], ['POST' => 0], null, false, false, null]],
        950 => [
            [['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null],
            [['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null],
        ],
        971 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        1017 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        1032 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        1053 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        1067 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        1078 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        1118 => [[['_route' => 'fos_user_security_login', '_controller' => 'fos_user.security.controller:loginAction'], ['_locale'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        1133 => [[['_route' => 'fos_user_security_check', '_controller' => 'fos_user.security.controller:checkAction'], ['_locale'], ['POST' => 0], null, false, false, null]],
        1146 => [[['_route' => 'fos_user_security_logout', '_controller' => 'fos_user.security.controller:logoutAction'], ['_locale'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        1166 => [[['_route' => 'fos_user_profile_show', '_controller' => 'fos_user.profile.controller:showAction'], ['_locale'], ['GET' => 0], null, true, false, null]],
        1183 => [[['_route' => 'fos_user_profile_edit', '_controller' => 'fos_user.profile.controller:editAction'], ['_locale'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        1208 => [[['_route' => 'fos_user_change_password', '_controller' => 'fos_user.change_password.controller:changePasswordAction'], ['_locale'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        1233 => [[['_route' => 'fos_user_registration_register', '_controller' => 'fos_user.registration.controller:registerAction'], ['_locale'], ['GET' => 0, 'POST' => 1], null, true, false, null]],
        1258 => [[['_route' => 'fos_user_registration_check_email', '_controller' => 'fos_user.registration.controller:checkEmailAction'], ['_locale'], ['GET' => 0], null, false, false, null]],
        1285 => [[['_route' => 'fos_user_registration_confirm', '_controller' => 'fos_user.registration.controller:confirmAction'], ['_locale', 'token'], ['GET' => 0], null, false, true, null]],
        1296 => [[['_route' => 'fos_user_registration_confirmed', '_controller' => 'fos_user.registration.controller:confirmedAction'], ['_locale'], ['GET' => 0], null, false, false, null]],
        1329 => [[['_route' => 'fos_user_resetting_request', '_controller' => 'fos_user.resetting.controller:requestAction'], ['_locale'], ['GET' => 0], null, false, false, null]],
        1350 => [[['_route' => 'fos_user_resetting_reset', '_controller' => 'fos_user.resetting.controller:resetAction'], ['_locale', 'token'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        1371 => [[['_route' => 'fos_user_resetting_send_email', '_controller' => 'fos_user.resetting.controller:sendEmailAction'], ['_locale'], ['POST' => 0], null, false, false, null]],
        1392 => [[['_route' => 'fos_user_resetting_check_email', '_controller' => 'fos_user.resetting.controller:checkEmailAction'], ['_locale'], ['GET' => 0], null, false, false, null]],
        1406 => [
            [['_route' => 'api'], ['_locale'], null, null, false, false, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
