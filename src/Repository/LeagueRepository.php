<?php

namespace App\Repository;

use App\Entity\League;
use App\Entity\MatchList;
use App\Entity\MatchResult;
use App\Entity\TenisPlayer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method League|null find($id, $lockMode = null, $lockVersion = null)
 * @method League|null findOneBy(array $criteria, array $orderBy = null)
 * @method League[]    findAll()
 * @method League[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeagueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, League::class);
    }

    public function getLeagueUserPointList(array $postData)
    {
        $result = ["success" => false, "message" => "No Action Taken", "data" => null];
        try {
            $leagueUserPointList = $this->createQueryBuilder("l")
                ->select("l.id")
                ->addSelect("l.leagueName")
                ->addSelect("player.playerName")
                ->addSelect("SUM(mr.winSet) as totalWinSet")
                ->addSelect("SUM(mr.loseSet) as totalLoseSet")
                ->addSelect("SUM(mr.setAvarage) as totalSetAvarage")
                ->addSelect("SUM(mr.winGame) as totalWinGame")
                ->addSelect("SUM(mr.loseGame) as totalLoseGame")
                ->addSelect("SUM(mr.gameAvarage) as totalGameAvarage")
                ->addSelect("SUM(mr.point) as totalWinPoint")
                ->addSelect("SUM(mr.winMatch) as totalWinMatchCount")
                ->leftJoin("l.matchLists", "ml")
                ->leftJoin("ml.matchResults", "mr")
                ->leftJoin("mr.player", "player")
                ->where("l.id=:league")
                ->setParameter("league", $postData["leagueId"])
                ->groupBy("mr.player")
                ->orderBy("mr.point","DESC")
                ->orderBy("mr.setAvarage","DESC")
                ->orderBy("mr.gameAvarage","DESC")
                ->getQuery()->getArrayResult();

            $result["success"] = true;
            $result["message"] = "Successfully";
            $result["data"] = $leagueUserPointList;
            return $result;
        } catch (\Exception $exception) {
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
            $result["data"] = [];
            return $result;
        }
    }
}
