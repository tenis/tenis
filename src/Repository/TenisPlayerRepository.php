<?php

namespace App\Repository;

use App\Entity\TenisPlayer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TenisPlayer|null find($id, $lockMode = null, $lockVersion = null)
 * @method TenisPlayer|null findOneBy(array $criteria, array $orderBy = null)
 * @method TenisPlayer[]    findAll()
 * @method TenisPlayer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TenisPlayerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenisPlayer::class);
    }

    public function getAllPlayer()
    {
        $result = ["success" => false, "message" => "No Action Taken", "data" => null];
        try {
            $userList = [];
            $users = $this->createQueryBuilder("u")
                ->select("u.id as id")
                ->addSelect("u.playerName as name")
                ->getQuery()->getArrayResult();
            $userList[0] = ["id" => "", "text" => "Oyuncu Seçiniz..."];
            foreach ($users as $user) {
                $userList[] = [
                    'id' => $user["id"],
                    'text' => $user["name"],
                ];
            }
            $result["success"] = true;
            $result["message"] = "Successfully";
            $result["data"] = $userList;
            return $result;
        } catch (\Exception $exception) {
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
            $result["data"] = [];
            return $result;
        }
    }
}
