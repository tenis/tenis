<?php

namespace App\Repository;

use App\Entity\League;
use App\Entity\MatchList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MatchList|null find($id, $lockMode = null, $lockVersion = null)
 * @method MatchList|null findOneBy(array $criteria, array $orderBy = null)
 * @method MatchList[]    findAll()
 * @method MatchList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatchListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MatchList::class);
    }

    /**
     * @param array $postData
     * @return array
     */
    public function newMatchList(array $postData, MatchResultRepository $matchResultRepository, League $league, EntityManagerInterface $em)
    {
        $result = ["success" => false, "message" => "No action taken", "errorMessage" => null];
        try {
            $matchList = new MatchList();
            $matchList
                ->setLeague($league)
                ->setName($postData["name"]);
            $em->persist($matchList);
            $productLangOptionLang = $matchResultRepository->newMatchResult($matchList, $postData, $em);
            if (!$productLangOptionLang["success"]) {
                return $productLangOptionLang;
            }
            try {
                $em->flush();
                $result["success"] = true;
                $result["message"] = "Creation Successfully";
                return $result;
            } catch (\Exception $exception) {
                $result["success"] = false;
                $result["message"] = $exception->getMessage();
                $result["errorMessage"] = "somethingWentWrong";
                return $result;
            }
        } catch (\Exception $exception) {
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
            $result["errorMessage"] = "somethingWentWrong";
            return $result;
        }
    }
}
