<?php

namespace App\Repository;

use App\Entity\MatchList;
use App\Entity\MatchResult;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MatchResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method MatchResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method MatchResult[]    findAll()
 * @method MatchResult[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatchResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MatchResult::class);
    }

    /**
     * @param MatchList $matchList
     * @param array $postData
     * @param EntityManagerInterface $em
     * @return array
     */
    public function newMatchResult(MatchList $matchList, array $postData,EntityManagerInterface $em)
    {
        $result = ["success" => false, "message" => "", "errorMessage" => null];
        if (count($postData) > 0) {
            foreach ($postData as $item) {
                try {
                    $matchResult = new MatchResult();
                    $matchResult
                        ->setMatchList($matchList)
                        ->setPoint($item["point"])
                        ->setWinSet($item["winSet"])
                        ->setGameAvarage($item["gameAvarage"])
                        ->setLoseGame($item["loseGame"])
                        ->setLoseSet($item["loseSet"])
                        ->setPlayer($em->find(User::class,(int)$item["player"]))
                        ->setSetAvarage($item["setAvarage"])
                        ->setWinGame($item["winGame"])
                        ->setWinMatch($item["winMatch"])
                    ;
                    $em->persist($matchResult);
                } catch (\Exception $exception) {
                    $result["success"] = false;
                    $result["message"] = $exception->getMessage();
                    $result["errorMessage"] = "somethingWentWrong";
                    return $result;
                }
            }
            $result["success"] = true;
            $result["message"] = "Creation Successfully";
            return $result;
        } else {
            $result["success"] = false;
            $result["message"] = "Not Found";
            $result["errorMessage"] = "notFound";
            return $result;
        }
    }
}
