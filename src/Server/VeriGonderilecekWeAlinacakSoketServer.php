<?php

namespace App\Server;

use App\Entity\Calls;
use Doctrine\ORM\EntityManagerInterface;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Entity\User;

// TODO: CLASS ADINI WE DOSYA ADINI DEĞİŞTİRİNİZ
class VeriGonderilecekWeAlinacakSoketServer implements MessageComponentInterface
{
    protected $connections = array();
    protected $getLastData = array();
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        echo "Serwer Başlatıldı \n";
    }

    /**
     * A new websocket connection
     *
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->connections[] = [
            "session" => $conn,
            "user" => "null"
        ];
        $data = [
            "username" => "null",
            "signalName" => "newConnection",
            "type" => "fix",
            "data" => "..:: Sokete Baplandınız ::..",
            "from" => "null"
        ];
        $conn->send(json_encode($data));
        echo "Yeni Soket Bağlantısı Kuruldu \n";
    }

    /**
     * Handle message sending
     *
     * @param ConnectionInterface $from
     * @param string $msg
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        echo $msg . "\n";
        // Gelen Data
        $messageData = json_decode(trim($msg));
        if (isset($messageData->type) && $this->isData($messageData->type)) {
            if ($messageData->type == "get") {
                // Kayıtlı Dataları Getirme
                if (isset($messageData->getUsername) && $this->isData($messageData->getUsername)) {
                    if (isset($messageData->getLastData) && $this->isData($messageData->getLastData)) {
                        if (isset($messageData->getSignalName) && $this->isData($messageData->getSignalName)) {
                            if (isset($messageData->allData) && $this->isData($messageData->allData)) {
                                if (isset($messageData->allUserData) && $this->isData($messageData->allUserData)) {
                                    $data = [
                                        "getUsername" => $messageData->getUsername,
                                        "getLastData" => $messageData->getLastData,
                                        "type" => "get",
                                        "allData" => $messageData->allData,
                                        "allUserData" => $messageData->allUserData,
                                        "getSignalName" => $messageData->getSignalName,
                                        "data" => "null",
                                    ];

                                    if ($messageData->allData == true) {
                                        $data["data"] = $this->getLastData;
                                    } else {
                                        if ($messageData->allUserData == true) {
                                            if (array_key_exists($messageData->getUsername, $this->getLastData)) {
                                                $data["data"] = $this->getLastData[$messageData->getUsername];
                                            } else {
                                                $data["data"] = $messageData->getUsername . " Kullanıcı Adına Ait Data Bulunamadı.";
                                            }
                                        } else {
                                            if (array_key_exists($messageData->getUsername, $this->getLastData)) {
                                                $data["data"] = $this->getLastData[$messageData->getUsername];
                                                if (array_key_exists($messageData->getSignalName, $this->getLastData[$messageData->getUsername])) {
                                                    $data["data"] = $this->getLastData[$messageData->getUsername][$messageData->getSignalName];
                                                } else {
                                                    $data["data"] = $messageData->getUsername . " Kullanıcı Adına Ait Sinyal Bulunamadı.";
                                                }
                                            } else {
                                                $data["data"] = $messageData->getUsername . " Kullanıcı Adına Ait Data Bulunamadı.";
                                            }
                                        }
                                    }
                                    $from->send(json_encode($data));
                                } else {
                                    $data = [
                                        "getUsername" => "Get Edeceğiniz Kişinin Kullanıcı Adını Yazınız.",
                                        "getLastData" => "Kesinlikle 'true' olmalıdır yoksa sistem hata verir",
                                        "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                                        "allData" => "Herkezin Tüm Sinyallerini Çekmek İstiyorsanız 'true' İstemiyorsanız 'false' Yapınız Eğer 'false' Yaparsanız 'allUserData' 'yı Doldurmanız Gerekir.",
                                        "allUserData" => "Bir Kullanıcının Tüm Sinyal Datalarını Almak İstiyorsanız 'true' İstemiyorsanız 'false' Gönderiniz Eğer 'false'  Yaparsanız 'sinyalName'  Vermelisiniz.",
                                        "getSignalName" => "Eğer 'allUserData' 'yı 'false' Yaptıysanız İsim Girmek Zorundasınız 'true' ise 'null' Yapınız.",
                                        "data" => "Gelen Data",
                                        "errorMessage" => "Lütfen 'allUserData' 'yı Yukarıda Belirtilen Şekilde Gönderiniz."
                                    ];
                                    $from->send(json_encode($data));
                                }
                            } else {
                                $data = [
                                    "getUsername" => "Get Edeceğiniz Kişinin Kullanıcı Adını Yazınız.",
                                    "getLastData" => "Kesinlikle 'true' olmalıdır yoksa sistem hata verir",
                                    "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                                    "allData" => "Herkezin Tüm Sinyallerini Çekmek İstiyorsanız 'true' İstemiyorsanız 'false' Yapınız Eğer 'false' Yaparsanız 'allUserData' 'yı Doldurmanız Gerekir.",
                                    "allUserData" => "Bir Kullanıcının Tüm Sinyal Datalarını Almak İstiyorsanız 'true' İstemiyorsanız 'false' Gönderiniz Eğer 'false'  Yaparsanız 'sinyalName'  Vermelisiniz.",
                                    "getSignalName" => "Eğer 'allUserData' 'yı 'false' Yaptıysanız İsim Girmek Zorundasınız 'true' ise 'null' Yapınız.",
                                    "data" => "Gelen Data",
                                    "errorMessage" => "Lütfen 'allData' 'yı Yukarıda Belirtilen Şekilde Gönderiniz."
                                ];
                                $from->send(json_encode($data));
                            }
                        } else {
                            $data = [
                                "getUsername" => "Get Edeceğiniz Kişinin Kullanıcı Adını Yazınız.",
                                "getLastData" => "Kesinlikle 'true' olmalıdır yoksa sistem hata verir",
                                "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                                "allData" => "Herkezin Tüm Sinyallerini Çekmek İstiyorsanız 'true' İstemiyorsanız 'false' Yapınız Eğer 'false' Yaparsanız 'allUserData' 'yı Doldurmanız Gerekir.",
                                "allUserData" => "Bir Kullanıcının Tüm Sinyal Datalarını Almak İstiyorsanız 'true' İstemiyorsanız 'false' Gönderiniz Eğer 'false'  Yaparsanız 'sinyalName'  Vermelisiniz.",
                                "getSignalName" => "Eğer 'allUserData' 'yı 'false' Yaptıysanız İsim Girmek Zorundasınız 'true' ise 'null' Yapınız.",
                                "data" => "Gelen Data",
                                "errorMessage" => "Lütfen 'signalName' 'i Yukarıda Belirtilen Şekilde Gönderiniz."
                            ];
                            $from->send(json_encode($data));
                        }
                    } else {
                        $data = [
                            "getUsername" => "Get Edeceğiniz Kişinin Kullanıcı Adını Yazınız.",
                            "getLastData" => "Kesinlikle 'true' olmalıdır yoksa sistem hata verir",
                            "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                            "allData" => "Herkezin Tüm Sinyallerini Çekmek İstiyorsanız 'true' İstemiyorsanız 'false' Yapınız Eğer 'false' Yaparsanız 'allUserData' 'yı Doldurmanız Gerekir.",
                            "allUserData" => "Bir Kullanıcının Tüm Sinyal Datalarını Almak İstiyorsanız 'true' İstemiyorsanız 'false' Gönderiniz Eğer 'false'  Yaparsanız 'sinyalName'  Vermelisiniz.",
                            "getSignalName" => "Eğer 'allUserData' 'yı 'false' Yaptıysanız İsim Girmek Zorundasınız 'true' ise 'null' Yapınız.",
                            "data" => "Gelen Data",
                            "errorMessage" => "Lütfen 'getLastData' 'yı Yukarıda Belirtilen Şekilde Gönderiniz."
                        ];
                        $from->send(json_encode($data));
                    }
                } else {
                    $data = [
                        "getUsername" => "Get Edeceğiniz Kişinin Kullanıcı Adını Yazınız.",
                        "getLastData" => "Kesinlikle 'true' olmalıdır yoksa sistem hata verir",
                        "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                        "allData" => "Herkezin Tüm Sinyallerini Çekmek İstiyorsanız 'true' İstemiyorsanız 'false' Yapınız Eğer 'false' Yaparsanız 'allUserData' 'yı Doldurmanız Gerekir.",
                        "allUserData" => "Bir Kullanıcının Tüm Sinyal Datalarını Almak İstiyorsanız 'true' İstemiyorsanız 'false' Gönderiniz Eğer 'false'  Yaparsanız 'sinyalName'  Vermelisiniz.",
                        "getSignalName" => "Eğer 'allUserData' 'yı 'false' Yaptıysanız İsim Girmek Zorundasınız 'true' ise 'null' Yapınız.",
                        "data" => "Gelen Data",
                        "errorMessage" => "Lütfen 'getUserName' 'i Yukarıda Belirtilen Şekilde Gönderiniz."
                    ];
                    $from->send(json_encode($data));
                }
            } elseif ($messageData->type == "send") {
                // Aktif Sinyal Üzerinden Data Gönderme
                if (isset($messageData->username) && $this->isData($messageData->username)) {
                    if (isset($messageData->signalName) && $this->isData($messageData->signalName)) {
                        if (isset($messageData->data) && $this->isData($messageData->data)) {
                            if (isset($messageData->from) && $this->isData($messageData->from)) {
                                if ($messageData->signalName != "session" || $messageData->signalName != "newConnection" || $messageData->signalName != "openConnection" || $messageData->signalName != "closeConnection" || $messageData->signalName != "error") {
                                    if (array_key_exists($messageData->signalName, $this->getLastData [$messageData->username])) {
                                        $data = [
                                            "username" => $messageData->username,
                                            "signalName" => $messageData->signalName,
                                            "type" => "send",
                                            "data" => $messageData->data,
                                            "from" => $messageData->from
                                        ];
                                        $this->getLastData [$messageData->username][$messageData->signalName] = $data;
                                    } else {
                                        $data = [
                                            "username" => $messageData->username,
                                            "signalName" => $messageData->signalName,
                                            "type" => "send",
                                            "data" => $messageData->signalName . " Böyle bir Sinyal Bulunamadı Önce Oluşturmalısınız.",
                                            "from" => $messageData->from
                                        ];
                                        $from->send(json_encode($data));
                                    }
                                } else {
                                    $data = [
                                        "username" => $messageData->username,
                                        "signalName" => $messageData->signalName,
                                        "type" => "create",
                                        "data" => "session, newConnection, openConnection, closeConnection, error Create Veriler Bunları İçeremez.",
                                        "from" => $messageData->from
                                    ];
                                    $from->send(json_encode($data));
                                }

                                // Standart Mesaj Gönderme
                                if ($messageData->signalName != "session" || $messageData->signalName != "newConnection" || $messageData->signalName != "openConnection" || $messageData->signalName != "closeConnection" || $messageData->signalName != "error") {
                                    if ($messageData->from == "null") {
                                        foreach ($this->connections as $key => $conn) {
                                            $conn["session"]->send(json_encode($data));
                                        }
                                    } else {
                                        foreach ($messageData->from as $item) {
                                            if (array_key_exists($item, $this->connections)) {
                                                $this->connections[$item]["session"]->send(json_encode($data));
                                            }
                                        }
                                    }
                                } else {
                                    $data = [
                                        "username" => $messageData->username,
                                        "signalName" => $messageData->signalName,
                                        "type" => "create",
                                        "data" => "session, newConnection, openConnection, closeConnection, error Create Veriler Bunları İçeremez.",
                                        "from" => $messageData->from
                                    ];
                                    $from->send(json_encode($data));
                                    $from->close();
                                }
                            } else {
                                $data = [
                                    "username" => "Kullanıcı Adınızı Giriniz.",
                                    "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                                    "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                                    "data" => "Array Bir Veri Giriniz",
                                    "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                                    "errorMessage" => "Lütfen 'from' 'u Yukarıda Belirtilen Şekilde Gönderiniz."
                                ];
                                $from->send(json_encode($data));
                                $from->close();
                            }
                        } else {
                            $data = [
                                "username" => "Kullanıcı Adınızı Giriniz.",
                                "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                                "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                                "data" => "Array Bir Veri Giriniz",
                                "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                                "errorMessage" => "Lütfen 'data' 'yı Yukarıda Belirtilen Şekilde Gönderiniz."
                            ];
                            $from->send(json_encode($data));
                            $from->close();
                        }
                    } else {
                        $data = [
                            "username" => "Kullanıcı Adınızı Giriniz.",
                            "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                            "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                            "data" => "Array Bir Veri Giriniz",
                            "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                            "errorMessage" => "Lütfen 'signalName' 'i Yukarıda Belirtilen Şekilde Gönderiniz."
                        ];
                        $from->send(json_encode($data));
                        $from->close();
                    }
                } else {
                    $data = [
                        "username" => "Kullanıcı Adınızı Giriniz.",
                        "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                        "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                        "data" => "Array Bir Veri Giriniz",
                        "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                        "errorMessage" => "Lütfen 'username' 'i Yukarıda Belirtilen Şekilde Gönderiniz."
                    ];
                    $from->send(json_encode($data));
                    $from->close();
                }
            } elseif ($messageData->type == "create") {
                // Sinyal Oluşturma
                if (isset($messageData->username) && $this->isData($messageData->username)) {
                    if (isset($messageData->signalName) && $this->isData($messageData->signalName)) {
                        if (isset($messageData->data) && $this->isData($messageData->data)) {
                            if (isset($messageData->from) && $this->isData($messageData->from)) {
                                if ($messageData->signalName != "session" || $messageData->signalName != "newConnection" || $messageData->signalName != "openConnection" || $messageData->signalName != "closeConnection" || $messageData->signalName != "error") {
                                    $data = [
                                        "username" => $messageData->username,
                                        "signalName" => $messageData->signalName,
                                        "type" => "create",
                                        "data" => $messageData->signalName . " Sinyali Oluşturulmuştur",
                                        "from" => $messageData->from
                                    ];
                                    if (array_key_exists($messageData->signalName, $this->getLastData[$messageData->username])) {
                                        $data["data"] = $messageData->signalName . " Aktif Sinyalinizi Kaldığınız Yerden Kullanabilirsiniz.";
                                    } else {
                                        $this->getLastData [$messageData->username][$messageData->signalName] = [];
                                    }
                                    $from->send(json_encode($data));
                                } else {
                                    $data = [
                                        "username" => $messageData->username,
                                        "signalName" => $messageData->signalName,
                                        "type" => "create",
                                        "data" => "session, newConnection, openConnection, closeConnection, error Create Veriler Bunları İçeremez.",
                                        "from" => $messageData->from
                                    ];
                                    $from->send(json_encode($data));
                                }
                            } else {
                                $data = [
                                    "username" => "Kullanıcı Adınızı Giriniz.",
                                    "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                                    "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                                    "data" => "Array Bir Veri Giriniz",
                                    "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                                    "errorMessage" => "Lütfen 'from' 'u Yukarıda Belirtilen Şekilde Gönderiniz."
                                ];
                                $from->send(json_encode($data));
                                $from->close();
                            }
                        } else {
                            $data = [
                                "username" => "Kullanıcı Adınızı Giriniz.",
                                "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                                "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                                "data" => "Array Bir Veri Giriniz",
                                "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                                "errorMessage" => "Lütfen 'data' 'yı Yukarıda Belirtilen Şekilde Gönderiniz."
                            ];
                            $from->send(json_encode($data));
                            $from->close();
                        }
                    } else {
                        $data = [
                            "username" => "Kullanıcı Adınızı Giriniz.",
                            "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                            "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                            "data" => "Array Bir Veri Giriniz",
                            "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                            "errorMessage" => "Lütfen 'signalName' 'i Yukarıda Belirtilen Şekilde Gönderiniz."
                        ];
                        $from->send(json_encode($data));
                        $from->close();
                    }
                } else {
                    $data = [
                        "username" => "Kullanıcı Adınızı Giriniz.",
                        "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                        "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                        "data" => "Array Bir Veri Giriniz",
                        "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                        "errorMessage" => "Lütfen 'username' 'i Yukarıda Belirtilen Şekilde Gönderiniz."
                    ];
                    $from->send(json_encode($data));
                    $from->close();
                }
            } elseif ($messageData->type == "fix") {
                // Register, UnRegister, Error İşlemleri
                if (isset($messageData->username) && $this->isData($messageData->username)) {
                    if (isset($messageData->signalName) && $this->isData($messageData->signalName)) {
                        if (isset($messageData->data) && $this->isData($messageData->data)) {
                            if (isset($messageData->from) && $this->isData($messageData->from)) {
                                if ($messageData->signalName != "session" || $messageData->signalName == "newConnection" || $messageData->signalName == "openConnection" || $messageData->signalName == "closeConnection" || $messageData->signalName == "error") {
                                    $data = [
                                        "username" => $messageData->username,
                                        "signalName" => $messageData->signalName,
                                        "type" => "fix",
                                        "data" => $messageData->data,
                                        "from" => $messageData->from
                                    ];
                                } else {
                                    $data = [
                                        "username" => $messageData->username,
                                        "signalName" => $messageData->signalName,
                                        "type" => "fix",
                                        "data" => "newConnection, openConnection, closeConnection, error Fix Veriler Bunların Dışına Çıkamaz.",
                                        "from" => $messageData->from
                                    ];
                                    $from->send(json_encode($data));
                                }

                                // Soket Session Bilgilerini Girme
                                if ($messageData->signalName == "openConnection") {
//                                    $user = $this->getUserByUsername($messageData->username);
//                                    if (!$user) {
//                                        $from->close();
//                                    } else {
                                        $keySes = array_search(["session" => $from, "user" => "null"], $this->connections);
                                        if ($keySes > -1) {
                                            unset($this->connections[$keySes]);
                                            $this->connections[$messageData->username] = [
                                                "session" => $from,
                                                "user" => $messageData->username
                                            ];
                                            if (array_key_exists($messageData->username, $this->getLastData)) {
                                                $data["data"] = "..:: ".$messageData->username." Olarak Kaldığınız Yerden Devam Edebilirsiniz ::..";
                                            }else{
                                                $this->getLastData [$messageData->username] = [];
                                            }
                                            if ($messageData->from == "null") {
                                                foreach ($this->connections as $key => $conn) {
                                                    $conn["session"]->send(json_encode($data));
                                                }
                                            } else {
                                                foreach ($messageData->from as $item) {
                                                    if (array_key_exists($item, $this->connections)) {
                                                        $this->connections[$item]["session"]->send(json_encode($data));
                                                    }
                                                }
                                            }
                                        }
//                                    }
                                }
                                // Soketten Ayrılma
                                if ($messageData->signalName == "closeConnection") {
                                    if ($messageData->from == "null") {
                                        foreach ($this->connections as $key => $conn) {
                                            $conn["session"]->send(json_encode($data));
                                        }
                                    } else {
                                        foreach ($messageData->from as $item) {
                                            if (array_key_exists($item, $this->connections)) {
                                                $this->connections[$item]["session"]->send(json_encode($data));
                                            }
                                        }
                                    }
                                    unset($this->connections[$messageData->username]);
                                    $from->close();
                                }
                                // Sokett Bağlantı Hatası
                                if ($messageData->signalName == "error") {
                                    $from->send(json_encode($data));
                                    $from->close();
                                }

                            } else {
                                $data = [
                                    "username" => "Kullanıcı Adınızı Giriniz.",
                                    "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                                    "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                                    "data" => "Array Bir Veri Giriniz",
                                    "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                                    "errorMessage" => "Lütfen 'from' 'u Yukarıda Belirtilen Şekilde Gönderiniz."
                                ];
                                $from->send(json_encode($data));
                                $from->close();
                            }
                        } else {
                            $data = [
                                "username" => "Kullanıcı Adınızı Giriniz.",
                                "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                                "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                                "data" => "Array Bir Veri Giriniz",
                                "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                                "errorMessage" => "Lütfen 'data' 'yı Yukarıda Belirtilen Şekilde Gönderiniz."
                            ];
                            $from->send(json_encode($data));
                            $from->close();
                        }
                    } else {
                        $data = [
                            "username" => "Kullanıcı Adınızı Giriniz.",
                            "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                            "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                            "data" => "Array Bir Veri Giriniz",
                            "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                            "errorMessage" => "Lütfen 'signalName' 'i Yukarıda Belirtilen Şekilde Gönderiniz."
                        ];
                        $from->send(json_encode($data));
                        $from->close();
                    }
                } else {
                    $data = [
                        "username" => "Kullanıcı Adınızı Giriniz.",
                        "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                        "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                        "data" => "Array Bir Veri Giriniz",
                        "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                        "errorMessage" => "Lütfen 'username' 'i Yukarıda Belirtilen Şekilde Gönderiniz."
                    ];
                    $from->send(json_encode($data));
                    $from->close();
                }
            } else {
                $data = [
                    "username" => "Kullanıcı Adınızı Giriniz.",
                    "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                    "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                    "data" => "Array Bir Veri Giriniz",
                    "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                    "errorMessage" => "Yukarıdaki veriler için 'type' Değişkeninizin datası 'create', 'send', 'fix' bu dataların dışına çıkamaz."
                ];
                $from->send(json_encode($data));
                $data = [
                    "getUsername" => "Get Edeceğiniz Kişinin Kullanıcı Adını Yazınız.",
                    "getLastData" => "Kesinlikle 'true' olmalıdır yoksa sistem hata verir",
                    "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                    "allData" => "Herkezin Tüm Sinyallerini Çekmek İstiyorsanız 'true' İstemiyorsanız 'false' Yapınız Eğer 'false' Yaparsanız 'allUserData' 'yı Doldurmanız Gerekir.",
                    "allUserData" => "Bir Kullanıcının Tüm Sinyal Datalarını Almak İstiyorsanız 'true' İstemiyorsanız 'false' Gönderiniz Eğer 'false'  Yaparsanız 'sinyalName'  Vermelisiniz.",
                    "getSignalName" => "Eğer 'allUserData' 'yı 'false' Yaptıysanız İsim Girmek Zorundasınız 'true' ise 'null' Yapınız.",
                    "data" => "Gelen Data",
                    "errorMessage" => "Yukarıdaki veriler için 'type' Değişkeninizin datası 'get' bu dataların dışına çıkamaz."
                ];
                $from->send(json_encode($data));
                $from->close();
            }
        } else {
            $data = [
                "username" => "Kullanıcı Adınızı Giriniz.",
                "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                "data" => "Array Bir Veri Giriniz",
                "from" => "Array Bir veri Giriniz Veya Herkeze Göndermek İçin 'null' gönderiniz",
                "errorMessage" => "Yukarıdaki veriler için 'type' Değişkeninizin datası 'create', 'send', 'fix' bu dataların dışına çıkamaz."
            ];
            $from->send(json_encode($data));
            $data = [
                "getUsername" => "Get Edeceğiniz Kişinin Kullanıcı Adını Yazınız.",
                "getLastData" => "Kesinlikle 'true' olmalıdır yoksa sistem hata verir",
                "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                "allData" => "Herkezin Tüm Sinyallerini Çekmek İstiyorsanız 'true' İstemiyorsanız 'false' Yapınız Eğer 'false' Yaparsanız 'allUserData' 'yı Doldurmanız Gerekir.",
                "allUserData" => "Bir Kullanıcının Tüm Sinyal Datalarını Almak İstiyorsanız 'true' İstemiyorsanız 'false' Gönderiniz Eğer 'false'  Yaparsanız 'sinyalName'  Vermelisiniz.",
                "getSignalName" => "Eğer 'allUserData' 'yı 'false' Yaptıysanız İsim Girmek Zorundasınız 'true' ise 'null' Yapınız.",
                "data" => "Gelen Data",
                "errorMessage" => "Yukarıdaki veriler için 'type' Değişkeninizin datası 'get' bu dataların dışına çıkamaz."
            ];
            $from->send(json_encode($data));
            $from->close();
        }
    }

    /**
     * A connection is closed
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        foreach ($this->connections as $key => $val) {
            if ($val["session"] == $conn) {
                $data = [
                    "username" => $key,
                    "signalName" => "connectionReject",
                    "type" => "fix",
                    "data" => "Soketten Atıldınız",
                    "from" => "null"
                ];
                $conn->send(json_encode($data));
                unset($this->connections[$key]);
                $conn->close();
            }
        }
    }

    /**
     * Error handling
     *
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $data = [
            "username" => "null",
            "signalName" => "error",
            "type" => "fix",
            "data" => "Error : " . $e->getMessage(),
            "from" => ["error"]
        ];
        $conn->send(json_encode($data));
        $conn->close();
    }


    protected function getUserByUsername($customer)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->container->get('doctrine')->getManager();
        $repo = $em->getRepository(User::class);

    }

    protected function toJson($data)
    {
        return $this->container->get('serializer')->serialize($data, 'json');
    }

    /**
     * @param $data
     * @return bool
     */
    public function isData($data)
    {
        if ($data != ""){
            if ($data != " "){
                if ($data != "  "){
                    if ($data != "undefined"){
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
