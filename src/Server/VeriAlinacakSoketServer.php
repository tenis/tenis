<?php

namespace App\Server;

use App\Entity\AcaLog;
use App\Entity\AcaType;
use App\Entity\BreakActivity;
use App\Entity\BreakType;
use App\Entity\Calls;
use App\Entity\Sales;
use App\Service\GetStatus;
use Doctrine\ORM\EntityManagerInterface;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use App\Entity\User;

// TODO: CLASS ADINI WE DOSYA ADINI DEĞİŞTİRİNİZ
class VeriAlinacakSoketServer implements MessageComponentInterface
{
    protected $connections = array();
    protected $instantMonitoring = array();
    protected $container;
    protected $translator;
    protected $getStatus;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->translator = $this->container->get("translator");
        $em = $this->container->get("doctrine.orm.entity_manager");
        $this->getStatus = new GetStatus($this->translator,$em);
        $this->sendData();
        echo "Serwer Başlatıldı \n";
    }

    /**
     * A new websocket connection
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->connections[] = $conn;
        $data = [
            "signalName" => "newConnection",
            "type" => "fix",
            "data" => "..:: Sokete Baplandınız ::..",
        ];
        $conn->send(json_encode($data));
        echo "Yeni Soket Bağlantısı Kuruldu \n";
    }

    /**
     * Handle message sending
     * @param ConnectionInterface $from
     * @param string $msg
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        echo $msg . "\n";
        // Gelen Data
        $messageData = json_decode(trim($msg));
        if (isset($messageData->type) && $this->isData($messageData->type)) {
            if ($messageData->type == "for"){
                $this->sendData();
            } elseif ($messageData->type == "get") {
                // Aktif Sinyal Üzerinden Data Gönderme
                if (isset($messageData->signalName) && $this->isData($messageData->signalName)) {
                    if (isset($messageData->data) && $this->isData($messageData->data)) {
                        if (isset($messageData->cusNo) && $this->isData($messageData->cusNo)) {
                            if ($messageData->signalName != "session" || $messageData->signalName != "newConnection" || $messageData->signalName != "openConnection" || $messageData->signalName != "closeConnection" || $messageData->signalName != "error") {
                                if ($messageData->signalName == "getInstantMonitoring"){
                                    $data = [
                                        "signalName" => "getInstantMonitoring",
                                        "cusNo" => $messageData->cusNo,
                                        "type" => "get",
                                        "data" => [],
                                    ];
                                    if (isset($this->instantMonitoring[$messageData->cusNo])){
                                        $data["data"]=$this->instantMonitoring[$messageData->cusNo];
                                    }
                                    $from->send(json_encode($data));
                                }
                            } else {
                                $data = [
                                    "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                                    "cusNo" => "Müşteri Numarasını Girmelisiniz",
                                    "type" => "data çağırmak için 'get' yazınız. register,unregister,error için 'fix' yazınız",
                                    "data" => "session, newConnection, openConnection, closeConnection, error Create Veriler Bunları İçeremez.",
                                ];
                                $from->send(json_encode($data));
                            }
                        }else {
                            $data = [
                                "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                                "cusNo" => "Müşteri Numarasını Girmelisiniz",
                                "type" => "data çağırmak için 'get' yazınız. register,unregister,error için 'fix' yazınız",
                                "data" => "Array Bir Veri Giriniz",
                            ];
                            $from->send(json_encode($data));
                        }
                    } else {
                        $data = [
                            "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                            "cusNo" => "Müşteri Numarasını Girmelisiniz",
                            "type" => "data çağırmak için 'get' yazınız. register,unregister,error için 'fix' yazınız",
                            "data" => "Array Bir Veri Giriniz",
                        ];
                        $from->send(json_encode($data));
                    }
                } else {
                    $data = [
                        "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                        "cusNo" => "Müşteri Numarasını Girmelisiniz",
                        "type" => "data çağırmak için 'get' yazınız. register,unregister,error için 'fix' yazınız",
                        "data" => "Array Bir Veri Giriniz",
                    ];
                    $from->send(json_encode($data));
                }
            } elseif ($messageData->type == "fix") {
                // Register, UnRegister, Error İşlemleri
                if (isset($messageData->signalName) && $this->isData($messageData->signalName)) {
                    if (isset($messageData->data) && $this->isData($messageData->data)) {
                        if (isset($messageData->cusNo) && $this->isData($messageData->cusNo)) {
                            $data = [
                                "signalName" => $messageData->signalName,
                                "cusNo" => $messageData->cusNo,
                                "type" => "fix",
                                "data" => $messageData->data,
                            ];
                            if ($messageData->signalName != "session" || $messageData->signalName == "newConnection" || $messageData->signalName == "openConnection" || $messageData->signalName == "closeConnection" || $messageData->signalName == "error") {

                                // Soket Session Bilgilerini Girme
                                if ($messageData->signalName == "openConnection") {
                                    $key = array_search($from, $this->connections);
                                    if ($key > -1) {
                                        unset($this->connections[$key]);
                                        $this->connections[$messageData->cusNo][] = $from;
                                    }elseif (isset($this->connections[$messageData->cusNo])){
                                        $key = array_search($from, $this->connections[$messageData->cusNo]);
                                        if ($key > -1) {
                                            unset($this->connections[$messageData->cusNo][$key]);
                                            $this->connections[$messageData->cusNo][] = $from;
                                        }else{
                                            $this->connections[$messageData->cusNo][] = $from;
                                        }
                                    }else{
                                        $this->connections[$messageData->cusNo][] = $from;
                                    }
                                    $data["data"] = "Başarı İle Oturumunuz Açılmıştır";
                                    $from->send(json_encode($data));
                                }
                                // Soketten Ayrılma
                                if ($messageData->signalName == "closeConnection") {
                                    $data["data"] = "Başarı İle Oturumunuz Kapatılmıştır";
                                    $from->send(json_encode($data));
                                    $key = array_search($from, $this->connections[$messageData->cusNo]);
                                    if ($key > -1) {
                                        unset($this->connections[$messageData->cusNo][$key]);
                                    }
                                    $from->close();
                                }
                                // Sokett Bağlantı Hatası
                                if ($messageData->signalName == "error") {
                                    $data["data"] = "Bağlantı Hatası Oluştu";
                                    $from->send(json_encode($data));
                                    $key = array_search($from, $this->connections[$messageData->cusNo]);
                                    if ($key > -1) {
                                        unset($this->connections[$messageData->cusNo][$key]);
                                    }
                                    $from->close();
                                }
                            } else {
                                $data["data"] = "newConnection, openConnection, closeConnection, error Fix Veriler Bunların Dışına Çıkamaz.";
                                $from->send(json_encode($data));
                                $from->close();
                            }
                        }else{
                            $data = [
                                "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                                "cusNo" => "Müşteri Numarasını Girmelisiniz",
                                "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                                "data" => "Array Bir Veri Giriniz",
                            ];
                            $from->send(json_encode($data));
                            $from->close();
                        }
                    } else {
                        $data = [
                            "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                            "cusNo" => "Müşteri Numarasını Girmelisiniz",
                            "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                            "data" => "Array Bir Veri Giriniz",
                        ];
                        $from->send(json_encode($data));
                        $from->close();
                    }
                } else {
                    $data = [
                        "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                        "cusNo" => "Müşteri Numarasını Girmelisiniz",
                        "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                        "data" => "Array Bir Veri Giriniz",
                    ];
                    $from->send(json_encode($data));
                    $from->close();
                }
            } else {
                $data = [
                    "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                    "cusNo" => "Müşteri Numarasını Girmelisiniz",
                    "type" => "data çağırmak için 'get' yeni bir sinyal oluşturmak için 'create' bir data göndermek için 'send' register,unregister,error için 'fix' yazınız",
                    "data" => "Array Bir Veri Giriniz",
                ];
                $from->send(json_encode($data));
                $from->close();
            }
        } else {
            $data = [
                "signalName" => "Olan Bir Sinyale Gönderebilirsiniz",
                "cusNo" => "Müşteri Numarasını Girmelisiniz",
                "type" => "data çağırmak için 'get' yazınız. register,unregister,error için 'fix' yazınız",
                "data" => "Array Bir Veri Giriniz",
            ];
            $from->send(json_encode($data));
        }
    }

    /**
     * A connection is closed
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        foreach ($this->connections as $key => $val) {
            $data = [
                "signalName" => "connectionReject",
                "cusNo" => $key,
                "type" => "fix",
                "data" => "Soketten Atıldınız",
            ];
            $conn->send(json_encode($data));
            $key = array_search($conn,$val);
            if ($key > -1){
                unset($val[$key]);
                $conn->close();
            }
        }
    }

    /**
     * Error handling
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        foreach ($this->connections as $key => $val) {
            $data = [
                "signalName" => "error",
                "cusNo" => $key,
                "type" => "fix",
                "data" => "Error : " . $e->getMessage(),
            ];
            $conn->send(json_encode($data));
            $key = array_search($conn,$val);
            if ($key > -1){
                unset($val[$key]);
                $conn->close();
            }
        }
    }

    protected function getUserByUsername($customer)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->container->get('doctrine')->getManager();
        $repo = $em->getRepository(User::class);

    }

    protected function toJson($data)
    {
        return $this->container->get('serializer')->serialize($data, 'json');
    }

    /**
     * @param $data
     * @return bool
     */
    public function isData($data)
    {
        if ($data != ""){
            if ($data != " "){
                if ($data != "  "){
                    if ($data != "undefined"){
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function sendData()
    {
        $statusList = $this->getStatus->status();
        $em = $this->container->get("doctrine.orm.entity_manager");
        $nowTime = new \DateTime();
        $sDate = $nowTime->format("Y-m-d 00:00:00");
        $eDate = $nowTime->format("Y-m-d 23:59:59");

        $sorgu_sonrasi_gonderilecek_array_data = [];

        $this->instantMonitoring = $sorgu_sonrasi_gonderilecek_array_data;

        foreach ($this->connections as $connKey=>$connVal){
            $dataRes = [
                "signalName" => "instantMonitoring",
                "cusNo" => $connKey,
                "type" => "get",
                "data" => [],
            ];
            if (isset($sorgu_sonrasi_gonderilecek_array_data[$connKey])){
                $dataRes["data"]=$sorgu_sonrasi_gonderilecek_array_data[$connKey];
            }
            foreach ($connVal as $conn){
                $conn->send(json_encode($dataRes));
            }
        }
    }
}
