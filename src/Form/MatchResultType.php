<?php

namespace App\Form;

use App\Entity\MatchResult;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MatchResultType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("player")
            ->add('winSet')
            ->add('loseSet')
            ->add('setAvarage')
            ->add('winGame')
            ->add('loseGame')
            ->add('gameAvarage')
            ->add('point')
            ->add('winMatch')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MatchResult::class,
        ]);
    }
}
