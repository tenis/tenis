<?php

namespace App\Form;

use App\Entity\MatchList;
use App\Entity\MatchResult;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MatchListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('league')
            ->add('name')
            ->add('matchResults', CollectionType::class, [
                'translation_domain' => 'messages',
                'label' => 'matchResults',
                'entry_type' => MatchResultType::class,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MatchList::class,
        ]);
    }
}
