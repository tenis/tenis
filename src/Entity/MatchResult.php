<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MatchResultRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=MatchResultRepository::class)
 */
class MatchResult
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $winSet;

    /**
     * @ORM\Column(type="integer")
     */
    private $loseSet;

    /**
     * @ORM\Column(type="integer")
     */
    private $setAvarage;

    /**
     * @ORM\Column(type="integer")
     */
    private $winGame;

    /**
     * @ORM\Column(type="integer")
     */
    private $loseGame;

    /**
     * @ORM\Column(type="integer")
     */
    private $gameAvarage;

    /**
     * @ORM\Column(type="integer")
     */
    private $point;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $winMatch;

    /**
     * @ORM\ManyToOne(targetEntity=TenisPlayer::class, inversedBy="matchResults")
     * @ORM\JoinColumn(nullable=false)
     */
    private $player;

    /**
     * @ORM\ManyToOne(targetEntity=MatchList::class, inversedBy="matchResults")
     * @ORM\JoinColumn(nullable=false)
     */
    private $matchList;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWinSet(): ?int
    {
        return $this->winSet;
    }

    public function setWinSet(int $winSet): self
    {
        $this->winSet = $winSet;

        return $this;
    }

    public function getLoseSet(): ?int
    {
        return $this->loseSet;
    }

    public function setLoseSet(int $loseSet): self
    {
        $this->loseSet = $loseSet;

        return $this;
    }

    public function getSetAvarage(): ?int
    {
        return $this->setAvarage;
    }

    public function setSetAvarage(int $setAvarage): self
    {
        $this->setAvarage = $setAvarage;

        return $this;
    }

    public function getWinGame(): ?int
    {
        return $this->winGame;
    }

    public function setWinGame(int $winGame): self
    {
        $this->winGame = $winGame;

        return $this;
    }

    public function getLoseGame(): ?int
    {
        return $this->loseGame;
    }

    public function setLoseGame(int $loseGame): self
    {
        $this->loseGame = $loseGame;

        return $this;
    }

    public function getGameAvarage(): ?int
    {
        return $this->gameAvarage;
    }

    public function setGameAvarage(int $gameAvarage): self
    {
        $this->gameAvarage = $gameAvarage;

        return $this;
    }

    public function getPoint(): ?int
    {
        return $this->point;
    }

    public function setPoint(int $point): self
    {
        $this->point = $point;

        return $this;
    }

    public function getWinMatch(): ?bool
    {
        return $this->winMatch;
    }

    public function setWinMatch(?bool $winMatch): self
    {
        $this->winMatch = $winMatch;

        return $this;
    }

    public function getPlayer(): ?TenisPlayer
    {
        return $this->player;
    }

    public function setPlayer(?TenisPlayer $player): self
    {
        $this->player = $player;

        return $this;
    }

    public function getMatchList(): ?MatchList
    {
        return $this->matchList;
    }

    public function setMatchList(?MatchList $matchList): self
    {
        $this->matchList = $matchList;

        return $this;
    }
}
