<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\LeagueRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=LeagueRepository::class)
 */
class League
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $leagueName;

    /**
     * @ORM\OneToMany(targetEntity=MatchList::class, mappedBy="league", orphanRemoval=true)
     */
    private $matchLists;

    public function __construct()
    {
        $this->matchLists = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getLeagueName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function setId(int $id): ?self
    {
         $this->id = $id;

         return  $this;
    }

    public function getLeagueName(): ?string
    {
        return $this->leagueName;
    }

    public function setLeagueName(string $leagueName): self
    {
        $this->leagueName = $leagueName;

        return $this;
    }

    /**
     * @return Collection|MatchList[]
     */
    public function getMatchLists(): Collection
    {
        return $this->matchLists;
    }

    public function addMatchList(MatchList $matchList): self
    {
        if (!$this->matchLists->contains($matchList)) {
            $this->matchLists[] = $matchList;
            $matchList->setLeague($this);
        }

        return $this;
    }

    public function removeMatchList(MatchList $matchList): self
    {
        if ($this->matchLists->removeElement($matchList)) {
            // set the owning side to null (unless already changed)
            if ($matchList->getLeague() === $this) {
                $matchList->setLeague(null);
            }
        }

        return $this;
    }
}
