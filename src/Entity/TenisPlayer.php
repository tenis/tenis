<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TenisPlayerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TenisPlayerRepository::class)
 */
class TenisPlayer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $playerName;

    /**
     * @ORM\OneToMany(targetEntity=MatchResult::class, mappedBy="player", orphanRemoval=true)
     */
    private $matchResults;

    public function __construct()
    {
        $this->matchResults = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getPlayerName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getPlayerName(): ?string
    {
        return $this->playerName;
    }

    public function setPlayerName(string $playerName): self
    {
        $this->playerName = $playerName;

        return $this;
    }

    /**
     * @return Collection|MatchResult[]
     */
    public function getMatchResults(): Collection
    {
        return $this->matchResults;
    }

    public function addMatchResult(MatchResult $matchResult): self
    {
        if (!$this->matchResults->contains($matchResult)) {
            $this->matchResults[] = $matchResult;
            $matchResult->setPlayer($this);
        }

        return $this;
    }

    public function removeMatchResult(MatchResult $matchResult): self
    {
        if ($this->matchResults->removeElement($matchResult)) {
            // set the owning side to null (unless already changed)
            if ($matchResult->getPlayer() === $this) {
                $matchResult->setPlayer(null);
            }
        }

        return $this;
    }
}
