<?php

namespace App\Controller\Front;

use App\Repository\CategoryRepository;
use App\Repository\LevelRepository;
use App\Repository\UserRepository;
use App\Repository\WordRepository;
use App\Service\MailProcessor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/api/mobil-function", name="mobil_")
 */
class MobilFunctionController extends AbstractController
{
    /**
     * @Route("/test", name="test", methods={"POST"})
     * @param Request $request
     * @param TokenStorageInterface $tokenStorage
     * @return JsonResponse
     */
    public function test(Request $request, TokenStorageInterface $tokenStorage)
    {
        $language = $request->getLocale();
        $actUser = $tokenStorage->getToken()->getUser();
        $postData = [];
        $jsonData = json_decode($request->getContent(), true);
        if (!is_null($jsonData)) $postData = $jsonData;
        $postData = array_merge($postData, $request->query->all());
        $postData = array_merge($postData, $request->request->all());
        $postData = array_merge($postData, $request->files->all());

        return $this->json(["sendPostUser" => $actUser, "lang" => $language, "data" => $postData]);
    }

    /**
     * @Route("/get-ip-address", name="get_ip_address", methods={"GET"})
     * @return Response
     */
    public function getIpAddress()
    {
        if (getenv("HTTP_CLIENT_IP")) {
            $ip = getenv("HTTP_CLIENT_IP");
        } elseif (getenv("HTTP_X_FORWARDED_FOR")) {
            $ip = getenv("HTTP_X_FORWARDED_FOR");
            if (strstr($ip, ',')) {
                $tmp = explode(',', $ip);
                $ip = trim($tmp[0]);
            }
        } else {
            $ip = getenv("REMOTE_ADDR");
        }
        return new Response($ip);
    }


    /**
     * @Route("/register", name="mobil_register", methods={"POST"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param UserRepository $userRepository
     * @param MailProcessor $mailProcessor
     * @return JsonResponse|Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder, UserRepository $userRepository,MailProcessor $mailProcessor)
    {
        $postData = [];
        $jsonData = json_decode($request->getContent(), true);
        if (!is_null($jsonData)) $postData = $jsonData;
        $postData = array_merge($postData, $request->query->all());
        $register = $userRepository->register($postData,$encoder);
        return $this->json($register);
    }


    /**
     * @Route("/change-password", name="mobil_change_password", methods={"POST"})
     * @param Request $request
     * @param TokenStorageInterface $tokenStorage
     * @param UserPasswordEncoderInterface $encoder
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function changePassword(Request $request, TokenStorageInterface $tokenStorage, UserPasswordEncoderInterface $encoder, UserRepository $userRepository)
    {
        $actUser = $tokenStorage->getToken()->getUser();
        $postData = [];
        $jsonData = json_decode($request->getContent(), true);
        if (!is_null($jsonData)) $postData = $jsonData;
        $postData = array_merge($postData, $request->query->all());
        $postData = array_merge($postData, $request->request->all());
        $postData = array_merge($postData, $request->files->all());

        $changePassword = $userRepository->changePassword($postData, $encoder, $actUser);

        return $this->json($changePassword);
    }


}
