<?php

namespace App\Controller\Front;

use App\Entity\Category;
use App\Entity\City;
use App\Entity\Order;
use App\Entity\OrderProduct;
use App\Entity\OrderState;
use App\Entity\Payment;
use App\Entity\PaymentMethod;
use App\Entity\Product;
use App\Entity\State;
use App\Entity\SuplierProduct;
use App\Entity\Supplier;
use App\Entity\User;
use App\Repository\CategoryRepository;
use App\Repository\MailRepository;
use App\Repository\NotificationRepository;
use App\Repository\OrderProductRepository;
use App\Repository\OrderRepository;
use App\Repository\ProductRepository;
use App\Repository\ProgressBillingRepository;
use App\Repository\SuplierProductRepository;
use App\Service\ImageProcessor;
use App\Service\MailProcessor;
use App\Service\SetTranslation;
use App\Service\StateProcessor;
use Knp\Component\Pager\PaginatorInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Yaml\Yaml;
use Symfony\Contracts\Translation\TranslatorInterface;
use function r\row;

/**
 * @Route("/test2", name="test_2_")
 */
class Test2Controller extends AbstractController
{
    /**
     * @param $data
     * @return false|string
     */
    public function toJson($data)
    {
        if (is_object($data)) {
            return $this->container->get('serializer')->serialize($data, 'json');
        } else {
            if (is_array($data)) {
                return $this->container->get('serializer')->serialize($data, 'json');
            } else {
                return json_encode([]);
            }
        }
    }

    /**
     * @Route("/get-ip-address", name="mobil_get_ip_address", methods={"GET"})
     * @return Response
     */
    public function getIpAddress()
    {
        if (getenv("HTTP_CLIENT_IP")) {
            $ip = getenv("HTTP_CLIENT_IP");
        } elseif (getenv("HTTP_X_FORWARDED_FOR")) {
            $ip = getenv("HTTP_X_FORWARDED_FOR");
            if (strstr($ip, ',')) {
                $tmp = explode(',', $ip);
                $ip = trim($tmp[0]);
            }
        } else {
            $ip = getenv("REMOTE_ADDR");
        }
        return $this->json($ip);
    }

    /**
     * @Route("/test2", name="test2")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request, ContainerInterface $container, ImageProcessor $imageProcessor, MailProcessor $mailProcessor)
    {

        return $this->render('test2/index.html.twig');
    }

//    /**
//     * @Route("/test2asdsadsa", name="test2asda" , methods={"GET","POST"})
//     * @param Request $request
//     * @param SetTranslation $setTranslation
//     * @return Response
//     */
//    public function asdsad(Request $request, SetTranslation $setTranslation,UserInterface $user, ProgressBillingRepository $progressBillingRepository, ContainerInterface $container, ImageProcessor $imageProcessor, MailProcessor $mailProcessor)
//    {
//        $language = $setTranslation->setLanguage($request->getPathInfo());
//        if ($language == "false") return new Response("We Don't This Language Option.");
//        $postData = [];
//        $jsonData = json_decode($request->getContent(), true);
//        if (!is_null($jsonData)) $postData = $jsonData;
//        $postData = array_merge($postData, $request->query->all());
//        $postData = array_merge($postData, $request->request->all());
//        $postData = array_merge($postData, $request->files->all());
////        $postData["supplier"] = $user->getSupplier()->getId();
//            $aaa=$progressBillingRepository->getSupplierProgressBillingVoteMonth($language,$postData);
////            dump($aaa);
////            exit();
//        return $this->render('test2/index.html.twig');
//    }

//    /**
//     * @Route("/test-ay/{value}", name="test_ay")
//     * @param Request $request
//     * @param SetTranslation $setTranslation
//     * @param int $value
//     * @return Response
//     */
//    public function index2(Request $request, SetTranslation $setTranslation, int $value)
//    {
//        $language = $setTranslation->setLanguage($request->getPathInfo());
//        if ($language == "false") return new Response("We Don't Have This Language Option.");
//        $em = $this->getDoctrine()->getManager();
////        foreach (range(1,100) as $item){
////            $supplier = new Supplier();
////            $supplier
////                ->setLogo("aaa")
////                ->setDescription("aaa")
////                ->setEmail("a@b.com")
////                ->setCommissionRate($item)
////                ->setAddress("awdawd")
////                ->setAffiliatedTaxOffice("awdawd")
////                ->setContactPerson("awdwad")
////                ->setGsmphone("awdawd")
////                ->setHrb("awdaw")
////                ->setIban("awdawd")
////                ->setLatitude("awd")
////                ->setLongitude("awdwa")
////                ->setManager("awdaw")
////                ->setMaxDistance(50)
////                ->setMinOrderValue(50)
////                ->setPhone("awdawd")
////                ->setPostCode($item)
////                ->setState($em->find(State::class,1))
////                ->setStreet("awdawd")
////                ->setTaxNumber("awdwdw")
////                ->setTaxOfficeOfficialName("aawdawd")
////                ->setUstNo("awdaw")
////                ->setCity($em->find(City::class,1))
////                ->setName("Tedarikçi_".$item);
////            $em->persist($supplier);
////        }
////        $em->flush();
//
//        $order = $em->getRepository(Order::class)->findAll();
//        /** @var Order $item */
//        foreach ($order as $key=>$item){
//            if (9000<$key){
//                $orderp = new OrderProduct();
//                $orderp
//                    ->setOrderr($item)
//                    ->setCommissionRate($value)
//                    ->setSuplierProduct($em->getRepository(SuplierProduct::class)->findOneBy(["supplier"=>$item->getSupplier(),"commissionRate"=>$value]))
//                    ->setKdv(5)
//                    ->setDeposit(5)
//                    ->setCommissionFreePrice(5000)
//                    ->setQuantity(5)
//                    ->setUnitPrice((10*$value));
//                $em->persist($orderp);
//            }
//        }
//        if (9000<$key) {
//            $em->flush();
//            if (101 > $value) {
//                return $this->redirectToRoute("test_ay", ["value" => $value + 1]);
//            }
//        }
//        return new Response("Bitti.");
//    }
//
//    /**
//     * @Route("/hnnnn",name="hnnnn")
//     * @param Request $request
//     * @param SetTranslation $setTranslation
//     * @param OrderRepository $orderRepository
//     * @return Response
//     */
//    public function xcxcxcxc(Request $request, SetTranslation $setTranslation,OrderRepository $orderRepository,PaginatorInterface $paginator)
//    {
//        $language = $setTranslation->setLanguage($request->getPathInfo());
//        if ($language == "false") return new Response("We Don't Have This Language Option.");
////        $em = $this->getDoctrine()->getManager();
////        $postData=$request->request->all();
////        $ff=$orderRepository->supplierProgressBillingDeneme($language,$postData,$paginator);
//        dump("asd");
//        exit();
//    }
//
//    /**
//     * @Route ("/service")
//     * @param StateProcessor $stateProcessor
//     *  @return Response
//     */
//    public function serviceControl(StateProcessor $stateProcessor): Response
//    {
//
//        return $this->render('test2/index.html.twig');
//
//    }
//
//    /**
//     * @Route ("/mail")
//     * @param Request $request
//     * @param SetTranslation $setTranslation
//     * @param MailRepository $mailRepository
//     * @param \Swift_Mailer $mailer
//     * @param \Twig\Environment $templating
//     * @param OrderProductRepository $orderProductRepository
//     * @param UserInterface $user
//     * @return Response
//     */
//    public function ordersmail(Request $request,SetTranslation $setTranslation,MailRepository $mailRepository,\Swift_Mailer $mailer,\Twig\Environment $templating,OrderProductRepository $orderProductRepository,UserInterface $user): Response
//    {
//        $language = $setTranslation->setLanguage($request->getPathInfo());
//        $em = $this->getDoctrine()->getManager();
//
//        $postData['order']=343;
//        $postData['owner']=$user;
//        $order = $orderProductRepository->getMyOrderDetails($language,$postData);
//        $mail = $mailRepository->orderSendMail($mailer,$templating,$postData);
//
////        $order = $em->find(Order::class, (int)$postData['order']);
////         $orders= $mailRepository->orderCustomerMail($language,$mailer,$templating,$postData,$orderProductRepository);
////        dump($orders);
////        exit();
////        $mailRepository->orderCustomerMail($mailer,$templating,$postData);
////        dump($order['data']['orderDetails']);
////        exit();
//        return $this->render("mailProcessor/ordersMail.html.twig",[
//            'orders'=>$order['data']['orderDetails'],
//        ]);
//    }

}
