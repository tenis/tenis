<?php

namespace App\Controller\Admin;

use App\Entity\TenisPlayer;
use App\Form\TenisPlayerType;
use App\Repository\TenisPlayerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tenis-player")
 */
class TenisPlayerController extends AbstractController
{
    /**
     * @Route("/", name="tenis_player_index", methods={"GET"})
     */
    public function index(TenisPlayerRepository $tenisPlayerRepository): Response
    {
        return $this->render('tenis_player/index.html.twig', [
            'tenis_players' => $tenisPlayerRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="tenis_player_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $tenisPlayer = new TenisPlayer();
        $form = $this->createForm(TenisPlayerType::class, $tenisPlayer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tenisPlayer);
            $entityManager->flush();

            return $this->redirectToRoute('tenis_player_index');
        }

        return $this->render('tenis_player/new.html.twig', [
            'tenis_player' => $tenisPlayer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tenis_player_show", methods={"GET"})
     */
    public function show(TenisPlayer $tenisPlayer): Response
    {
        return $this->render('tenis_player/show.html.twig', [
            'tenis_player' => $tenisPlayer,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="tenis_player_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TenisPlayer $tenisPlayer): Response
    {
        $form = $this->createForm(TenisPlayerType::class, $tenisPlayer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tenis_player_index');
        }

        return $this->render('tenis_player/edit.html.twig', [
            'tenis_player' => $tenisPlayer,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="tenis_player_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TenisPlayer $tenisPlayer): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tenisPlayer->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tenisPlayer);
            $entityManager->flush();
        }

        return $this->redirectToRoute('tenis_player_index');
    }
}
