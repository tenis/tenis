<?php

namespace App\Controller\Admin;

use App\Entity\League;
use App\Entity\MatchList;
use App\Entity\MatchResult;
use App\Entity\TenisPlayer;
use App\Form\MatchList1Type;
use App\Form\MatchListType;
use App\Form\MatchResultType;
use App\Repository\MatchListRepository;
use App\Repository\MatchResultRepository;
use App\Repository\TenisPlayerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/match-list")
 */
class MatchListController extends AbstractController
{
    /**
     * @Route("/", name="match_list_index", methods={"GET"})
     */
    public function index(MatchListRepository $matchListRepository): Response
    {
        return $this->render('match_list/index.html.twig', [
            'match_lists' => $matchListRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="match_list_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $matchList = new MatchList();
        $matchList->addMatchResult(
            (new MatchResult())
                ->setWinMatch(false)
                ->setWinGame(0)
                ->setSetAvarage(0)
                ->setPlayer(null)
                ->setLoseSet(0)
                ->setLoseGame(0)
                ->setGameAvarage(0)
                ->setPoint(0)
                ->setWinSet(0)
        );
        $matchList->addMatchResult(
            (new MatchResult())
                ->setWinMatch(false)
                ->setWinGame(0)
                ->setSetAvarage(0)
                ->setPlayer(null)
                ->setLoseSet(0)
                ->setLoseGame(0)
                ->setGameAvarage(0)
                ->setPoint(0)
                ->setWinSet(0)
        );

        $form = $this->createForm(MatchListType::class, $matchList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($matchList);
            $em->flush();
            return $this->redirectToRoute("match_list_index");
        }

        return $this->render('match_result/new.html.twig', [
            'match_list' => $matchList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="match_list_show", methods={"GET"})
     */
    public function show(MatchList $matchList): Response
    {
        return $this->render('match_list/show.html.twig', [
            'match_list' => $matchList,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="match_list_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, MatchList $matchList): Response
    {
        $form = $this->createForm(MatchListType::class, $matchList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('match_list_index');
        }

        return $this->render('match_list/edit.html.twig', [
            'match_list' => $matchList,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="match_list_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MatchList $matchList): Response
    {
        if ($this->isCsrfTokenValid('delete'.$matchList->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($matchList);
            $entityManager->flush();
        }

        return $this->redirectToRoute('match_list_index');
    }
}
