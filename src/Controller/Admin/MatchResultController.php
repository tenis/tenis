<?php

namespace App\Controller\Admin;

use App\Entity\MatchResult;
use App\Form\MatchResultType;
use App\Repository\MatchResultRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/match-result")
 */
class MatchResultController extends AbstractController
{
    /**
     * @Route("/", name="match_result_index", methods={"GET"})
     */
    public function index(MatchResultRepository $matchResultRepository): Response
    {
        return $this->render('match_result/index.html.twig', [
            'match_results' => $matchResultRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="match_result_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $matchResult = new MatchResult();
        $form = $this->createForm(MatchResultType::class, $matchResult);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($matchResult);
            $entityManager->flush();

            return $this->redirectToRoute('match_result_index');
        }

        return $this->render('match_result/new.html.twig', [
            'match_result' => $matchResult,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="match_result_show", methods={"GET"})
     */
    public function show(MatchResult $matchResult): Response
    {
        return $this->render('match_result/show.html.twig', [
            'match_result' => $matchResult,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="match_result_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, MatchResult $matchResult): Response
    {
        $form = $this->createForm(MatchResultType::class, $matchResult);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('match_result_index');
        }

        return $this->render('match_result/edit.html.twig', [
            'match_result' => $matchResult,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="match_result_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MatchResult $matchResult): Response
    {
        if ($this->isCsrfTokenValid('delete'.$matchResult->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($matchResult);
            $entityManager->flush();
        }

        return $this->redirectToRoute('match_result_index');
    }
}
