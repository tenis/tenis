<?php

namespace App\Service;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MailProcessor
{
    /** @var ContainerInterface  */
    private $container;

    /** @var \Swift_Mailer */
    private $mailer;

    /** @var \Twig\Environment */
    private $templating;

    public function __construct(ContainerInterface $container, \Swift_Mailer $mailer, \Twig\Environment $templating)
    {
        $this->container = $container;
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    /**
     * @param string $lang
     * @param array $postData
     * @return array
     */
    public function forgotPassword(string $lang, array $postData)
    {
        $result = ["success" => false, "message" => "No action taken"];
        try {
            if (isset($postData["mails"]) && is_array($postData["mails"]) && count($postData["mails"]) > 0){
                if (isset($postData["url"]) && is_string($postData["url"]) && strlen($postData["url"]) > 0){
                    $message = (new \Swift_Message("İyi Fiyat - Yeni Şifre"))
                        ->setFrom('noreply@iyifiyat.com')
                        ->setTo($postData["mails"])
                        ->setBody($postData["url"]);
                    $this->mailer->send($message);

                    $result["success"] = true;
                    $result["message"] = "successfully";
                }else{
                    $result["success"] = false;
                    $result["message"] = "Password is string and not space length > 0";
                }
            }else{
                $result["success"] = false;
                $result["message"] = "Mail is not array or array in item not found";
            }
        }catch (\Exception $exception){
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
        }
        return $result;
    }
}
