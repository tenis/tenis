<?php

namespace App\Service;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class DataConverterProcessor
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $data
     * @return array
     */
    public function toJsonString($data)
    {
        $result = ["success" => false, "message" => "No action taken", "data" => json_encode([])];
        try {
            if (is_object($data)) {
                $result["data"] = $this->container->get('serializer')->serialize($data, 'json');
            } else {
                if (is_array($data)) {
                    $result["data"] = $this->container->get('serializer')->serialize($data, 'json');
                } else {
                    $result["data"] = json_encode([]);
                }
            }
            $result["success"] = true;
            $result["message"] = "successfully";
        } catch (\Exception $exception) {
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
            $result["data"] = json_encode([]);
        }
        return $result;
    }

    /**
     * @param array $data
     * @param $index
     * @param $column
     * @return array
     */
    public function vueSelect2DataToJsonString(array $data = [], $index, $column)
    {
        $result = ["success" => false, "message" => "No action taken", "data" => json_encode([])];
        try {
            $res = [];
            if (count($data) > 0) {
                foreach ($data as $item) {
                    $res[] = [
                        "id" => $item[$index],
                        "text" => $item[$column]
                    ];
                }
            }
            $data = $this->toJsonString($res);
            if ($data["success"]) {
                $data = $data["data"];
                $result["success"] = true;
                $result["message"] = "successfully";
                $result["data"] = $data;
            } else {
                $result = $data;
            }
        } catch (\Exception $exception) {
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
            $result["data"] = json_encode([]);
        }
        return $result;
    }

    /**
     * @param array $data
     * @param $index
     * @param $column
     * @return array
     */
    public function vueSelect2DataToJson(array $data = [], $index, $column)
    {
        $result = ["success" => false, "message" => "No action taken", "data" => json_encode([])];
        try {
            $res = [];
            if (count($data) > 0) {
                foreach ($data as $item) {
                    $res[] = [
                        "id" => $item[$index],
                        "text" => $item[$column]
                    ];
                }
            }
            $result["success"] = true;
            $result["message"] = "successfully";
            $result["data"] = $res;
        } catch (\Exception $exception) {
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
            $result["data"] = json_encode([]);
        }
        return $result;
    }
}
