<?php

namespace App\Service;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Palette\RGB;
use Imagine\Image\Point;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageProcessor
{
    /** @var ContainerInterface  */
    private $container;

    /** @var Imagine  */
    private $imagine;

    /** @var Base64FileExtractor  */
    private $base64FileExtractor;

    public function __construct(ContainerInterface $container, Base64FileExtractor $base64FileExtractor)
    {
        $this->container = $container;
        $this->imagine = new Imagine();
        $this->base64FileExtractor = $base64FileExtractor;
    }

    /**
     * @param array $postData
     */
    public function isData(array $postData)
    {
        $result = ["success"=>false,"message"=>"No action taken"];
        if (count($postData)>0){
            try {
//                /** @var UploadedFile $item */
//                foreach ($postData as $item){
//                    if (!is_null($item["image"])){
//                        $size = $this->imagine->open($item["image"]->getPathname())->getSize();
//                        if ($size->getWidth() < 750){
//                            $result["success"] = false;
//                            $result["message"] = "Image width is too small (".$size->getWidth()."px). The minimum expected width is 750 pixels.";
//                            return $result;
//                        }
//                        if ($size->getHeight() < 750){
//                            $result["success"] = false;
//                            $result["message"] = "Image height is too small (".$size->getHeight()."px). The minimum expected height is 750 pixels.";
//                            return $result;
//                        }
//                    }
//                }
                $result["success"] = true;
                $result["message"] = "Data validation successful";
                return $result;
            }catch (\Exception $exception){
                $result["success"] = false;
                $result["message"] = $exception->getMessage();
                return $result;
            }
        }else{
            $result["success"] = false;
            $result["message"] = "You should send a picture (file or base64)";
            return $result;
        }
    }

    /**
     * @param UploadedFile|null $file
     * @param string $base64File
     * @return array
     */
    public function saveImage(UploadedFile $file=null, string $base64File="")
    {
        $result = ["success"=>false, "message"=>"No action taken", "data"=>null];
        if ($base64File == ""){
            if (is_null($file)){
                $result["success"] = false;
                $result["message"] = "You should send a picture (file or base64)";
                return $result;
            }else{
                try {
                    $saveImageSize = $this->saveImageSize($file);
                    if ($saveImageSize["success"]){
                        $result["success"] = true;
                        $result["message"] = "Uploaded Successfully";
                        $result["data"] = $saveImageSize["data"];
                    }else{
                        return $saveImageSize;
                    }
                }catch (\Exception $exception){
                    $result["success"] = false;
                    $result["message"] = $exception->getMessage();
                    return $result;
                }
            }
        }else{
            try {
                $base64Image = $this->base64FileExtractor->extractBase64String($base64File);
                $imageFile = new UploadedBase64File($base64Image, "blabla");
                $saveImageSize = $this->saveImageSize($imageFile);
                if ($saveImageSize["success"]){
                    $result["success"] = true;
                    $result["message"] = "Uploaded Successfully";
                    $result["data"] = $saveImageSize["data"];
                }else{
                    return $saveImageSize;
                }
            }catch (\Exception $exception){
                $result["success"] = false;
                $result["message"] = $exception->getMessage();
                return $result;
            }
        }

        return $result;
    }

    /**
     * @param UploadedFile $file
     * @return array
     */
    public function saveImageSize(UploadedFile $file)
    {
        $result = ["success"=>false,"message"=>"No action taken", "data"=>null];
        try {
            $fileName = md5(uniqid()).".".$file->guessExtension();
            $file->move($this->container->getParameter("uploadDirectory")."/original",$fileName);

            $data = [
                [
                    "fileName" => "small",
                    "imageName" => $fileName,
                    "imageSize" => 750
                ],
                [
                    "fileName" => "square",
                    "imageName" => $fileName,
                    "imageSize" => 200
                ],
                [
                    "fileName" => "tiny",
                    "imageName" => $fileName,
                    "imageSize" => 100
                ],
                [
                    "fileName" => "thumb",
                    "imageName" => $fileName,
                    "imageSize" => 50
                ]
            ];

            foreach ($data as $postData){
                $saveImageItem = $this->saveImageItem($postData);
                if (!$saveImageItem["success"]){
                    return $saveImageItem;
                }
            }

            $result["success"] = true;
            $result["message"] = "Image Saved Successful";
            $result["data"] = $fileName;
        }catch (\Exception $exception){
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
        }
        return $result;
    }

    /**
     * @param array $postData
     * @return array
     */
    public function saveImageItem(array $postData)
    {
        $result = ["success"=>false,"message"=>"No action taken", "data"=>null];
        try {
            $size = $this->imagine->open($this->container->getParameter("uploadDirectory")."/original/".$postData["imageName"])->getSize();
            if ($size->getWidth() < $size->getHeight()){
                $imageSize = $size->heighten($postData["imageSize"]);
            }else{
                $imageSize = $size->widen($postData["imageSize"]);
            }

            $palette = new RGB();
            $color = $palette->color("#fff",0);
            $createBox = new Box($postData["imageSize"],$postData["imageSize"]); //TODO: KUTU AÇTIM

            $image = $this->imagine
                ->open($this->container->getParameter("uploadDirectory")."/original/".$postData["imageName"])
                ->thumbnail($imageSize,"inset")
                ->copy(); //TODO: RESMİ UZUN KENARINI $postData["imageSize"] YAPIP KOPYALADIM

            $ortHeight = 0; //TODO: ORTALAMALAR İÇİN DEĞİŞKEN OLUŞTURDUM
            $ortWith = 0; //TODO: ORTALAMALAR İÇİN DEĞİŞKEN OLUŞTURDUM
            if ($image->getSize()->getWidth() < $image->getSize()->getHeight()){
                $ortWith = ($postData["imageSize"] - $image->getSize()->getWidth()) / 2; //TODO: RESMİN ORTASINI BULUYORUM
                if ($image->getSize()->getHeight() < $postData["imageSize"]){
                    $ortHeight = ($postData["imageSize"] - $image->getSize()->getHeight()) / 2; //TODO: RESMİN ORTASINI BULUYORUM
                }
            }else{
                $ortHeight = ($postData["imageSize"] - $image->getSize()->getHeight()) / 2; //TODO: RESMİN ORTASINI BULUYORUM
                if ($image->getSize()->getWidth() < $postData["imageSize"]){
                    $ortWith = ($postData["imageSize"] - $image->getSize()->getWidth()) / 2; //TODO: RESMİN ORTASINI BULUYORUM
                }
            }

            $pointer = new Point($ortWith,$ortHeight); //TODO: ORTASINI TANITIYORUM
            $this->imagine
                ->create($createBox,$color)
                ->paste($image,$pointer,100)
                ->save($this->container->getParameter("uploadDirectory")."/".$postData["fileName"]."/".$postData["imageName"]);//TODO: AÇTIĞIM KUTUYA YAPIŞTIRIP KAYDEDİYORUM

            $result["success"] = true;
            $result["message"] = "Image Saved Successful";
            $result["data"] = $postData["imageName"];
        }catch (\Exception $exception){
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
        }
        return $result;
    }

    /**
     * @param int $key
     * @return array
     */
    public function updateAllImage(int $key)
    {
        $result = ["success"=>false,"message"=>"No action taken", "data"=>null];
        try {
            $files = scandir($this->container->getParameter("uploadDirectory")."/original");
            $start = $key;
            $index = 50;
            if ((count($files) - $key) < 50){
                $index = (count($files) - $key);
                if ($index < 0){
                    $index = 0;
                }
            }
            $end = $key + $index;
            $files = array_slice($files,$start, $index);
            foreach ($files as $fileName) {
                if ($fileName == '.' || $fileName == '..' || $fileName == 'README.md') continue;

                $data = [
                    [
                        "fileName" => "tiny",
                        "imageName" => $fileName,
                        "imageSize" => 100
                    ],
                    [
                        "fileName" => "thumb",
                        "imageName" => $fileName,
                        "imageSize" => 50
                    ]
                ];

                foreach ($data as $postData){
                    $saveImageItem = $this->saveImageItem($postData);
                    if (!$saveImageItem["success"]){
                        return $saveImageItem;
                    }
                }
            }
            $result["success"] = true;
            $result["message"] = "Image Saved Successful";
            $result["data"] = [
                "end" => $end,
                "index" => $index
            ];
        }catch (\Exception $exception){
            $result["success"] = false;
            $result["message"] = $exception->getMessage();
        }
        return $result;
    }

    /**
     * @param string $imageName
     * @return array
     */
    public function deleteImage(string $imageName)
    {
        $result = ["success"=>false,"message"=>"No action taken"];
        $arrayMessage = [];
        try {
            unlink($this->container->getParameter("uploadDirectory")."/original/".$imageName);
            $arrayMessage ["original"] = "Deleted";
        }catch (\Exception $exception){
            $arrayMessage ["original"] = "Not Deleted";
        }
        try {
            unlink($this->container->getParameter("uploadDirectory")."/small/".$imageName);
            $arrayMessage ["small"] = "Deleted";
        }catch (\Exception $exception){
            $arrayMessage ["small"] = "Not Deleted";
        }
        try {
            unlink($this->container->getParameter("uploadDirectory")."/square/".$imageName);
            $arrayMessage ["square"] = "Deleted";
        }catch (\Exception $exception){
            $arrayMessage ["square"] = "Not Deleted";
        }
        try {
            unlink($this->container->getParameter("uploadDirectory")."/tiny/".$imageName);
            $arrayMessage ["tiny"] = "Deleted";
        }catch (\Exception $exception){
            $arrayMessage ["tiny"] = "Not Deleted";
        }
        try {
            unlink($this->container->getParameter("uploadDirectory")."/thumb/".$imageName);
            $arrayMessage ["thumb"] = "Deleted";
        }catch (\Exception $exception){
            $arrayMessage ["thumb"] = "Not Deleted";
        }
        $result["success"] = true;
        $result["message"] = json_encode($arrayMessage);
        return $result;
    }
}
